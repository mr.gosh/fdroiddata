Categories:Games
License:GPLv3+
Web Site:https://github.com/raatmarien/Point-and-Hit/blob/HEAD/README.md
Source Code:https://github.com/raatmarien/Point-and-Hit
Issue Tracker:https://github.com/raatmarien/Point-and-Hit/issues

Auto Name:Point & Hit
Summary:Fast-paced tilt-based game
Description:
Shoot the targets before they disappear! Explore different themes by reaching
higher scores! Point & Hit has fast addicting gameplay and a build in tutorial.
.

Repo Type:git
Repo:https://github.com/raatmarien/Point-and-Hit

Build:1.0.2,10
    commit=v1.0.2
    subdir=android
    gradle=yes

Build:1.0.3,11
    commit=v1.0.3
    subdir=android
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.0.3
Current Version Code:11
