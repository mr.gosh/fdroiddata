Categories:Security
License:Apache2
Web Site:
Source Code:https://github.com/chrismiceli/motp
Issue Tracker:https://github.com/chrismiceli/motp/issues

Auto Name:mOTP
Summary:One-time password generator
Description:
Generating otp's without the need for a special hardware token. Using motp, hotp
or totp algorithms, wih multiple profiles and time-zone support
.

Repo Type:git
Repo:https://github.com/chrismiceli/motp

Build:1.5,20
    commit=4
    target=android-15

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.5
Current Version Code:20
