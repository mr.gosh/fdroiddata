Categories:System
License:MIT
Web Site:https://github.com/raatmarien/red-moon/blob/HEAD/README.md
Source Code:https://github.com/raatmarien/red-moon
Issue Tracker:https://github.com/raatmarien/red-moon/issues

Auto Name:Red Moon
Summary:Screen filter for night time phone use
Description:
Screen filter app with seperate color temperature, intensity and filter level
settings. It has a persistent notification with controls while running and is
able to automatically start on boot.
.

Repo Type:git
Repo:https://github.com/raatmarien/red-moon

Build:2.1,4
    commit=v2.1
    subdir=app
    gradle=yes

Build:2.2,5
    commit=v2.2
    subdir=app
    gradle=yes

Build:2.2.1,6
    commit=v2.2.1
    subdir=app
    gradle=yes

Build:2.2.3,8
    commit=v2.2.3
    subdir=app
    gradle=yes

Build:2.3.0,9
    commit=v2.3.0
    subdir=app
    gradle=yes

Build:2.5.0,11
    commit=v2.5.0
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:2.5.0
Current Version Code:11
